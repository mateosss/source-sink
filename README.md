# Sink Source Protocol (SSP)

Implementación de un algoritmo simple de capa de transporte que resuelve los
problemas de flujo y congestión del modelo dado.

# Informe

El informe se encuentra en informe/informe.md y también hay una versión en pdf.

# Clases

- Generator - Aplicación emisora generando grandes cantidades de datos a enviar
- Queue - Cualquier buffer que reciba, procese, y entregue paquetes
- Sink - Aplicación receptora
- TransportTx - Hereda de Queue, capa de transporte del emisor
- TransportRx - Hereda de Queue, capa de transporte del receptor
- DataPacket - Paquete de datos de SSP con `seqnum`
- FeedbackPacket - Paquete de feedback de SSP con `acknum` y `window size`
