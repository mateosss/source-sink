#ifndef TRANSPORTTX
#define TRANSPORTTX

#include <string.h>
#include <omnetpp.h>
#include "Queue.h"
#include "SSPPacket_m.h"

using namespace omnetpp;

class TransportTx: public Queue {
private:
    // Flow control
    unsigned int canSend; // Amount of slots remaining in the receptor window
    unsigned int remainsToConfirm; // Amount of packets that still weren't confirmed

    // Congestion control
    unsigned int cwFloor; // cw lower bound
    unsigned int cwCeil; // cw upper bound
    unsigned int cw; // Congestion window size
    unsigned int cwToSend; // Packets to send in this cw
    unsigned int cwRecv; // Packets of this cw received
    bool lookingForCeil; // True at slow-start part of the algorithm
    bool shouldBackoff; // True if cw has passed the network capacity
    unsigned int sequenceNumber; // Current packet being sent, similar to deliveredPackets
    unsigned int acknowledUpTo; // Last acknowledged packet
    simtime_t feedbackTimeout; // If a feedback was not sent after this time, assume that Rx received everything
    cOutVector cwStat; // Stats for cw size

public:
    TransportTx();
    virtual ~TransportTx();
protected:
    virtual void initialize();

    virtual cPacket* deliverPacket();
    virtual bool attendDataPacket(cMessage *msg);
    virtual void attendFeedbackPacket(cMessage *msg);

    virtual void cwUpdate();
    virtual void bumpFeedbackTimeout();
    virtual void recoverFromFeedbackTimeout();
};

Define_Module(TransportTx);

TransportTx::TransportTx()
    : canSend(0),
      remainsToConfirm(0),
      cwFloor(0),
      cwCeil(0),
      cw(0),
      cwToSend(0),
      cwRecv(0),
      lookingForCeil(false),
      shouldBackoff(false),
      sequenceNumber(0),
      acknowledUpTo(0) {
}

TransportTx::~TransportTx() { }

void TransportTx::initialize() {
    Queue::initialize();
    cwStat.setName("Congestion Window Size");
    deliverThrough = "port$o";
    canSend = 1;
    remainsToConfirm = 0;

    cwFloor = 0;
    cw = cwToSend = cwCeil =  1;
    cwStat.record(cw);
    cwRecv = 0;
    lookingForCeil = true;
    shouldBackoff = false;

    sequenceNumber = 1;
    feedbackTimeout = SIMTIME_MAX;
}

cPacket* TransportTx::deliverPacket() {
    // Check for feedback timeout
    if (simTime() >= feedbackTimeout) recoverFromFeedbackTimeout();

    // Stop sending packets if receptor window may be full, or the cw was reached
    if (canSend <= 0 || cwToSend == 0) return nullptr;

    // Deliver packet normally
    canSend--;
    remainsToConfirm++;
    cwToSend--;
    return Queue::deliverPacket();
}

bool TransportTx::attendDataPacket(cMessage *msg) {
    // Add sequenceNumber
    bool enqueued = Queue::attendDataPacket(msg);
    if (enqueued) static_cast<DataPacket*>(msg)->setSequenceNumber(sequenceNumber++);
    return enqueued;
}

void TransportTx::attendFeedbackPacket(cMessage *msg) {
    bumpFeedbackTimeout();
    FeedbackPacket *feedback = static_cast<FeedbackPacket*>(msg);
    unsigned int ack = feedback->getAcknowledgeNumber();
    unsigned int ackDistance = ack - acknowledUpTo;

    // Should adapt to feedback's remainingBuffer?
    if (canSend == 0 && remainsToConfirm == 1) canSend = feedback->getRemainingBuffer();
    else canSend++;

    // If any packet was lost, backoff cw on cwUpdate
    if (ackDistance > 1) shouldBackoff = true;

    remainsToConfirm -= ackDistance;
    cwRecv += ackDistance;
    acknowledUpTo = ack;
    printf("[<--] cw=%u, cwToSend=%u, cwRecv=%u, remainsToConfirm=%u\n", cw, cwToSend, cwRecv, remainsToConfirm); fflush(stdout);

    // Should congestion window be updated? (No more to send and all received)
    if (cwToSend == 0 && cwRecv == cw) cwUpdate();

    printf("      canSend=%u; remainsToConfirm=%u; RXAck=%u\n", canSend, remainsToConfirm, ack); fflush(stdout);
    delete feedback;
}

void TransportTx::cwUpdate() {
    if (lookingForCeil) {
        if (!shouldBackoff) {
            cwFloor = cwCeil;
            cw = cwCeil *= 2;
        } else {
            cw = (cwFloor + cwCeil) / 2;
            lookingForCeil = false;
        }
    } else {
        if (shouldBackoff) cwCeil = cw;
        else cwFloor = cw;
        cw = (cwFloor + cwCeil) / 2;
    }
    cwToSend = cw;
    cwRecv = 0;
    shouldBackoff = false;
    cwStat.record(cw);
}

void TransportTx::bumpFeedbackTimeout() {
    feedbackTimeout = simTime() + SimTime(1000, SIMTIME_MS);
}

// Resets flow and congestion information after some hardcoded time without feedback
// Assumes it was lost because of congestion
void TransportTx::recoverFromFeedbackTimeout() {
    canSend += remainsToConfirm;
    remainsToConfirm = 0;
    cwUpdate();
    bumpFeedbackTimeout();
}

#endif /* TRANSPORTTX */
