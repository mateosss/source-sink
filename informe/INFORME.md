# Sink Source Protocol (SSP)

***Algoritmo distribuído que minimiza pérdida de paquetes maximizando caudal de
transferencia en un modelo particular de source-sink***

## Abstracto

En este trabajo presentamos el desarrollo de `SSP`, un algoritmo novel de capa
de transporte que resuelve un problema muy particular al que denominaremos
*source-sink*, se aplican técnicas de control de flujo y de congestión para
maximizar el caudal de transferencia, minimizando pérdidas que ocurrirán en
casos de saturación de las entidades del modelo. Presentaremos dos casos de
estudio que representan momentos en los que estas pérdidas ocurren, y
mostraremos como `SSP` efectivamente logra mejorar la situación inicial.

## Introducción

**Situación:** Existe una entidad transmisora `TX`, que desea transmitir a una
receptora `RX` a través de un paso intermedio `NX`, mediante un medio con cierta
capacidad de transmisión. Asumiendo que todos los entes del sistema son cerrados
e inalterables, salvo por la propia transmisión, con una *"memoria"*  de tamaño
fijo. A este modelo lo llamaremos el modelo *"source-sink"* o `SS`

**Problema:** Se desea optimizar la velocidad de transmisión y minimizar la
pérdida de información que ocurriría al saturar estas memorias. El principal
problema es uno de comunicación entre `TX` con `NX` y `RX`, en donde el primero
no tiene información sobre los demás, entonces, por defecto, transmite a máxima
velocidad, generando que las memorias de las entidades se saturen y pierdan
datos.

## Trabajos Relacionados

Un buen modelo en el que basarse es el protocolo TCP, el cual resuelve un
problema más genérico que este, pero igualmente muy similar. Para esto TCP
divide el problema en dos, con los siguientes algoritmos que lo intentan
solucionar:
- Control de Flujo: Parada y espera, Retroceso N y Repetición selectiva
- Control de Congestión: Arranque lento, Talhoe, Reno, Vegas, BIC, Compound,
  CUBIC, etc

De varios de estos algoritmos tomaremos ideas, y los analizaremos con base en
los problemas de flujo y congestión.

## Organización del Experimento

### Modelo

Se utilizará el framework `Omnet++` para simular el modelo `SS`,
generando el siguiente network:

![Modelo en Omnet++](img/model.png)

En este modelo, las queues *(o "colas")* `TransportTx`*(a partir de ahora
`Tx`)*, `queueForward`, `queueBackward` *(ambas `Nx`)* y `TransportTx` *(`Rx`)*
representarán, con un buffer la memoria de las distintas entidades, con
`queueForward` y `queueBackward` representando `NX`. Las conexiones con
`datarate` configurable serán el medio por el que los datos viajarán. Lo que se
transmitirá serán paquetes de `12500 Bytes` (`100Kb`).

El modelo tendrá configuraciones iniciales, que por defecto, no generarán
problemas, y por ende nos interesa estudiar, pero a saber: `TX` tendrá un buffer
size de tamaño cuasi ilimitado, el cuál siempre tendrá paquetes disponibles
(para esto `Generator` generará paquetes a grandes velocidades), por otro lado
`NX` y `RX` tendrán buffers de tamaño **200**. Por último, los datarate de las
conexiones serán todos **1Mbps**.

### Definición de los Casos de Estudio

Así como TCP utiliza los problemas de control de flujo y de congestión como
bases para los distintos algoritmos que especifica, nosotros los utilizaremos
para analizar cómo el modelo es afectado por estos problemas, e intentar
mejorar la situación implementando un algoritmo novel.

- **Caso 1:** Para el problema de congestión de flujo definiremos una
  configuración inicial llamada `Caso 1` o `C1`, en el cual queremos acentuar
  los problemas de tener un `RX` lento/con poca memoria, para esto modificaremos
  el modelo inicial, disminuyendo el tamaño del buffer de `RX` a **100** y el
  datarate de la conexión entre`Rx` y `Sink` a **0.5Mbps**

- **Caso 2:** Por otro lado, para modelar el problema de congestión, queremos
  analizar el caso en que los agentes mediadores son lentos/de poca memoria y
  por ende, modificaremos el modelo inicial, disminuyendo el tamaño del buffer
  de `NX` (`queueForward`) a **100**, y la conexión entre `queueForward` y `Rx`
  a **0.5Mbps**

## Análisis de los casos de Estudio sin Algoritmo

Al correr el modelo antes de la implementación de `SSP`, el transmisor emite
la mayor cantidad de paquetes posible, siendo limitado únicamente por el
datarate de la conexión `Tx --> queueForward`. Es sencillo ver que al estudiar
el `Caso 1` y el `Caso 2` surgirán problemas similares, en donde los buffers
de `Rx` y `queueForward` (`Nx`) respectivamente serán saturados. En ambos casos se observará una pérdida de paquetes y delay (tiempo de vida) de los mismos casi idéntico.

Dejaremos los gráficos comparativos para la sección de *resultados* en donde
se podrán observar estos comportamientos, sobrepuestos con los de `SSP`.

## Métodos

A continuación se explicará el proceso de pensamiento que se tuvo a la hora de
diseñar e implementar `SSP`. Muchas de las ideas del algoritmo se basan en
aspectos utópicos del modelo, que pueden llegar a ser útiles en un escenario
real, pero son ciertamente una desventaja si lo que se busca es un algoritmo que
soporte modelos más dinámicos. `SSP` además no implementa retransmisiones ya que
no son necesarias para el estudio del problema, en un escenario real es
necesario implementarlas. Es suficiente con saber cuantos paquetes se pierden,
para saber que tan eficiente será una vez se implemente retransmisión.

Tener en cuenta que nuestro modelo garantiza que:

- No hay otras entidades además de `TX` y `RX` intentando comunicarse: los
 buffers son de uso exclusivo para esta transmisión.

- Las conexiones están libres de defectos: tienen capacidades de transmisión
  constantes y no producen pérdida de datos por fallas en la conexión.

### Control de Flujo en `SSP`

El primer y más simple problema a resolver fue el de control de flujo, cómo
hacer que un `Tx` rápido no sature a un `Rx` lento y de poca memoria. Utilizamos
la idea de `repetición selectiva`, con un tamaño de ráfaga máximo. Para esto
definimos un paquete de feedback que `Rx` enviará a `Tx` anunciando cuanto
buffer le queda:

```cpp
packet FeedbackPacket {
    unsigned int remainingBuffer;
}
```

`Tx` enviará un único primer paquete, y esperará a que vuelva con la
información del tamaño del buffer de `Rx`, a partir de aquí, se envían todos los
paquetes que se pueda, teniendo cuidado de no sobrepasar el buffer que `Rx`
anunció. De esta forma el `Caso 1` queda resuelto. Sin embargo es sencillo
ver que el `Caso 2` falla miserablemente, ya que `Tx` solo tiene información
de la ventana de `Rx` y no de `Nx`, y como el modelo no nos permite comunicar
`Nx` con `Tx` debemos buscar otra forma de resolver este problema.

La implementación de `Tx` tiene esta pinta, suponiendo que existen eventos
`attendFeedbackPacket` *(que es ejecutado cuando llega a `Tx` un `FeedbackPacket`
de `Rx`)* y `deliverDataPacket` *(cuando `Tx` intenta enviar un
`DataPacket`)*.

```cpp
class Tx: Queue {
  unsigned int canSend = 0;
  unsigned int remainsToConfirm = 0;

  void attendFeedbackPacket() {
    Queue::attendFeedbackPacket();
    if (canSend == 0 && remainsToConfirm == 1)
      canSend = feedback->getRemainingBuffer();
    else canSend++;
    remainsToConfirm--;
  }

  void deliverDataPacket() {
    if (canSend == 0) return; // Can't send
    Queue::deliverDataPacket();
    canSend--;
    remainsToConfirm++;
  }
}
```
*Esta clase es `TransportTx` en la implementación de prueba*

### Control de Congestión en `SSP`

Para el control de congestión, se utilizaron ideas similares a las de `arranque
lento` y `TCP Reno`, utilizando una ventana de congestión `cw` que funciona como
otra cota superior, además de la del control de flujo, de paquetes a enviar.

En nuestro modelo tenemos una ventaja en comparación a `TCP` y es que podemos
suponer que el datarate es constante, y que no hay otras entidades
congestionando el `NX`. Por ende podemos buscar dos cotas con arranque lento y
luego hacer una búsqueda binaria para encontrarlo. En esta búsqueda asumimos que
podemos seguir aumentando la `cw` si no hay pérdida de paquetes, y si las hay,
hay que disminuir la `cw`.

![Ventana de Congestión](img/cw.png)

*`SSP` buscando la cw óptima, primero con arranque lento hasta encontrar cota
superior, y luego con búsqueda lineal.*


Una idea de cómo se actualiza la `cw` puede verse en esta porción de la
implementación:

```cpp
// true mientras se hace arranque lento
bool lookingForCeil = true;
// true cuando ocurre un packet drop con la cw actual
bool shouldBackoff = false;
// cw y las cotas para la búsqueda
unsigned int cwFloor = 0, cwCeil = 1, cw = 1;
// Cuántos paquetes de esta cw restan por llegar
unsigned int cwRecv = 0;

void TransportTx::cwUpdate() {
    if (lookingForCeil) {
        if (!shouldBackoff) {
            cwFloor = cwCeil;
            cw = cwCeil *= 2;
        } else {
            cw = (cwFloor + cwCeil) / 2;
            lookingForCeil = false;
        }
    } else {
        if (shouldBackoff) cwCeil = cw;
        else cwFloor = cw;
        cw = (cwFloor + cwCeil) / 2;
    }
    cwToSend = cw;
    cwRecv = 0;
    shouldBackoff = false;
}
```

## Resultados

Los resultados se analizan sobre una simulación de 200 segundos, y muestran
como varían distintos aspectos a medida que la simulación avanza.

### Caso 1

Al analizar el caso de control de flujo, que tiene un `Rx` que se satura,
podemos observar como `SSP` se comporta en comparación con el modelo inicial
sin algoritmo.

![Buffer Rx](img/rxbuffer.png)

Se ve como en el caso sin algoritmo, el buffer se satura rápidamente, y luego se
mantiene lleno. Si bien esto podría parecer bueno por que se utiliza por más
tiempo el buffer completo, la realidad es que esto genera inmensas cantidades de
pérdidas de paquetes, como veremos a continuación. En la curva con `SSP`, por
otro lado, se observa como el buffer se llena cada vez más, a medida que la
`Tx` va la congestión de la red y aumentando la `cw`.

### Caso 2

Similarmente para el `Caso 2` de control de congestión nos enfocamos en el
buffer `Nx` (`queueForward`)

![Buffer Nx](img/nxbuffer.png)

El gráfico es muy similar al anterior, con un pequeño detalle, en el caso
con `SSP` hay momentos en los que el buffer realmente se satura, a diferencia
del `Caso 1` donde `Rx` nunca llegaba a saturarse. Esto es por que
el control de flujo dentro de `SSP` impide a `Tx` saturar al receptor, pero no
sucede lo mismo con `Nx`, el cual no tiene forma de comunicar su ventana a `Tx`.

### Pérdida de Paquetes (C1 y C2)

Es necesario aclarar, que la curva muestra en el eje Y los paquetes perdidos
hasta el momento, y por ende es siempre creciente, y marca pérdida, solo en
los momentos donde efectivamente crece.

![Pérdida de Paquetes Acumulativa](img/packetdrops.png)

Como vemos, ambos casos sin `SSP` pierden una cantidad inmensa de paquetes, con
`SSP` por otro lado, en el `C2` se pierden apenas unos cuantos *(unas 10 veces
menos)*. Notar que no hay curva para `C2` con `SSP` ya que este caso no pierde
ningún paquete gracias al control de flujo que implementa.

### Delay de Paquetes (C1 y C2)

Para el análisis del delay, es necesario recordad que en el caso sin algoritmo
el delay es muy bajo, ya que los únicos paquetes que llegan a registrarlos son
los que no fueron perdidos, si existiese retransmisión, como es probable que
haya en un escenario real, el delay sería peor en todos los casos.

Notar además que nos interesa mostrar el delay que había en las etapas tempranas
del desarrollo de `SSP` cuando solo había control de flujo (`Flow`), en este
caso el delay es el mismo que sin en la primera parte, y luego, o continúa
linealmente en los momentos donde sin algoritmo, habría pérdida de paquetes
(`C1`),  o simplemente actúa igual que sin algoritmo (`C2`).

![Delay Acumulativo](img/delay.png)

El algoritmo completo de `SSP` es el más penalizado con el delay
ya que tiene muchas etapas de pausa en donde debe esperar que todos los paquetes
de la ventana de congestión hayan vuelto.

### Variaciones en la Carga Ofrecida

A continuación queremos analizar cómo el algoritmo escala a distintos valores de
datarates entre `Tx` y `Nx` en comparación al modelo inicial, y ver si a la
larga, la pérdida de velocidad es muy grande. Notar que estos gráficos utilizan
algún caso analizado anteriormente *(`C1` o `C2`)*, sin embargo, es indiferente
que caso se utiliza, ya que estos solo afectan partes posteriores de la
transmisión.

Tener en cuenta entonces que en *datarate de 0.5Mbps*, es en donde comienza a
haber saturación de bufferes.

#### Delay

Lo primero que analizaremos será el delay promedio de los paquetes durante la
simulación. Es lógico ver que el delay tiende a disminuir a medida que el
datarate del transmisor aumenta, pero es interesante analizar cómo el algoritmo
empeora un poco esta curva.

![Delay Según Datarate](img/datarate_delay.png)

Al comenzar a tener problemas de congestión o flujo en 0.5Mbps, vemos
como el algoritmo, aumenta drásticamente la derivada de descenso, haciéndola
casi 0.

### Carga Útil (Goodput)

Y por último este gráfico muestra como `SSP`, a la larga, tiene el mismo goodput
que sin algoritmo, con la diferencia, de que la pérdida de paquetes es 10 veces
menor. El eje Y representa el promedio de paquetes enviados por segundo en los
200 segundos de la simulación.

![Goodput Según Datarate.png](img/datarate_goodput.png)

## Discusión

`SSP` está lejos de ser un algoritmo para uso en escenarios reales, por que
asume precondiciones difíciles de replicar en casos concretos.

Traeremos de vuelta las dos asunciones más fuertes de `SSP` y veremos como
estas ramifican en problemas más serios.

### Sólo existen Tx, Nx y Rx

Asumiendo esto, podemos saber que los buffers son de uso exclusivo para
esta transferencia, por ende no hay que preocuparse por buffers que varíen.
Para el control de flujo esto es necesario, por que si `Rx` anuncia una ventana,
`Tx` ignora información posterior que `Rx` pueda dar, ya que esta tiene mucho
delay en comparación a la velocidad de entrega de paquetes. Para el control de
congestión, podemos saber que una vez que encontramos la ventana de congestión,
esta nunca va a cambiar, ya que no hay otros transmisores saturando el mismo
`Nx` que estoy utilizando.

### Las conexiones no tienen fallas

`SSP` asume que la única forma en la que un paquete puede desaparecer, es por
congestión de buferes, si a esto habría que sumarle fallas físicas, el algoritmo
que calcula ventana de congestión, no podría basarse en pérdida de paquetes para
saber que hay congestión. Esto se asume de manera similar en TCP, sin embargo,
en TCP, no se utilizan algoritmos de congestión que acoten estrictamente la
`cw`, `SSP` por otro lado, actualiza pone cotas inferiores y superiores
estrictas que solo achican el intervalo en el que la `cw` puede encontrase, esto
lo hace inservible para redes en donde la tasa de transferencia no sea
constante.

### Siempre suficientes datos en Tx

Si el algoritmo de congestión dictara que la `cw` es de 1024, pero la `Tx` está
casi vacía y el `Generator` genera a un ritmo muy lento los paquetes. `Tx` será
capaz de enviar los 1024 paquetes lentamente, y al no detectar pérdida de
paquetes subiría la `cw` a 2048 sin saber realmente si la red soporta 1024
paquetes enviados a máxima velocidad.

## Trabajo Futuro

Una opción es hacer que `SSP` funcione mejor en casos más genéricos, pero para
esto creemos que TCP ya es suficientemente bueno, así que en cambio, en trabajos
posteriores se hará foco en mejorar aún más la performance en este modelo en
particular. Las principales características a mejorar serían:

- No es necesario enviar `remainingBuffer` en `FeedbackPacket`s que no sean el
  primero ya que estos son ignorados.

- Al saber que los paquetes siempre están en orden correcto, podemos no solo
  saber cuando hay pérdida de paquetes comparando el número de secuencia que
  `Tx` espera contra el que marca el feedback que recibió, si no además, podemos
  saber *cuántos* paquetes se perdieron, es decir, hay lugar para implementar
  alguna heurística que nos permita saber *que tan errada está la `cw`* y no
  solo si lo está. Esto haría casi instantáneo encontrar la `cw` correcta.

## Referencias

- *A. S. Tanembaum - Computer Networks 5th Edition*
- *Kurose, J. F. and Ross, K. W. Computer Networking - A Top Down Approach. 7th
  Edition*
- *[Web Server Performance with CUBIC and Compound TCP](https://iu4.ru/docs/web-server-performance.pdf)*
- [TCP Congestion Control - Wikipedia EN](https://en.wikipedia.org/wiki/TCP_congestion_control)
