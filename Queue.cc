#ifndef QUEUE
#define QUEUE

#include <string.h>
#include <omnetpp.h>

#include "Queue.h"

#include "SSPPacket_m.h"

using namespace omnetpp;

Queue::Queue() {
    endServiceEvent = NULL;
    droppedPackets = 0;
    deliveredPackets = 0;
    lastSampleDelivered = 0;
}

Queue::~Queue() {
    cancelAndDelete(endServiceEvent);
    cancelAndDelete(throughputSampleEvent);
}

void Queue::initialize() {
    buffer.setName("buffer");
    endServiceEvent = new cMessage("endService", INTERNAL_PACKET);
    throughputSampleEvent = new cMessage("throughputSample", INTERNAL_PACKET);
    bufferSizeVector.setName("BufferSize");
    packetDropVector.setName("PacketDrop");
    throughput.setName("Throughput");

    deliverThrough = "out";
    throughputSampleTime = par("throughputSampleRate");
    scheduleAt(simTime() + throughputSampleTime, throughputSampleEvent);
}

void Queue::finish() { }

void Queue::handleMessage(cMessage *msg) {
    if (msg == endServiceEvent) deliverPacket();
    else if (msg == throughputSampleEvent) sampleThroughput();
    else if (msg->getKind() == DATA_PACKET) attendDataPacket(msg);
    else if (msg->getKind() == FEEDBACK_PACKET) attendFeedbackPacket(msg);
}

// Amount of packets this buffer has space for
int Queue::getRemainingBuffer()
{
    return par("bufferSize").intValue() - buffer.getLength();
}

// Pops a packet from the buffer and returns it or nullptr if none
cPacket* Queue::deliverPacket() {
    // Delivers the next packet if any
    if (buffer.isEmpty()) return nullptr;
    // dequeue and deliver
    cPacket *pkt = (cPacket*) buffer.pop();
    send(pkt, deliverThrough);
    deliveredPackets++;

    // Deliver next packet in serviceTime seconds
    serviceTime = pkt->getDuration();
    scheduleAt(simTime() + serviceTime, endServiceEvent);
    return pkt;
}

void Queue::sampleThroughput() {
    throughput.record(deliveredPackets - lastSampleDelivered);
    lastSampleDelivered = deliveredPackets;
    scheduleAt(simTime() + throughputSampleTime, throughputSampleEvent);
}

// Returns true if the packet was enqueued, false if it was dropped
bool Queue::attendDataPacket(cMessage *msg) {
    if (getRemainingBuffer() <= 0) { // Buffer full, drop packet
        delete msg;
        this->bubble("packet dropped");
        packetDropVector.record(++droppedPackets);
        return false;
    } else { // Enqueue the packet for deliver
        buffer.insert(msg);
        bufferSizeVector.record(buffer.getLength());
        if (!endServiceEvent->isScheduled()) // if the server is idle
            scheduleAt(simTime() + static_cast<DataPacket*>(msg)->getDuration(), endServiceEvent); // TODO: Instead of +0, should be +serviceTime
        return true;
    }
}

void Queue::attendFeedbackPacket(cMessage *msg) {
    // By default, queues pass feedback packet as if they where normal data packets
    Queue::attendDataPacket(msg);
}

#endif /* QUEUE */
