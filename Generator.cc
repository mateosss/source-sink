#ifndef GENERATOR
#define GENERATOR

#include <string.h>
#include <omnetpp.h>

#include "SSPPacket_m.h"

using namespace omnetpp;

class Generator : public cSimpleModule {
private:
    cMessage *sendMsgEvent;

    // Stats
    unsigned int generatedPackets;
public:
    Generator();
    virtual ~Generator();
protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);
    virtual void generate();
};
Define_Module(Generator);

Generator::Generator() : sendMsgEvent(nullptr), generatedPackets(0) { }

Generator::~Generator() {
    cancelAndDelete(sendMsgEvent);
}

void Generator::generate() {
    DataPacket *pkt = new DataPacket();
    pkt->setByteLength(par("packetByteSize"));
    send(pkt, "out");
    generatedPackets++;
}

void Generator::initialize() {
    sendMsgEvent = new cMessage("sendEvent", INTERNAL_PACKET);
    scheduleAt(par("generationInterval"), sendMsgEvent);
}

void Generator::finish() {
    recordScalar("Number of generated packets", generatedPackets);
}

void Generator::handleMessage(cMessage *msg) {
    generate();

    simtime_t departureTime = simTime() + par("generationInterval");
    scheduleAt(departureTime, sendMsgEvent);
}

#endif /* GENERATOR */
