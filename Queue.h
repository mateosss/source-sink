#ifndef QUEUE_H_
#define QUEUE_H_

#include <string.h>
#include <omnetpp.h>

using namespace omnetpp;

class Queue: public cSimpleModule {
protected:
    cQueue buffer; // Stores packets
    cMessage *endServiceEvent; // Signals that a packet can be delivered
    simtime_t serviceTime; // Time that takes to attend the most recent packet
    cMessage *throughputSampleEvent; // Fires a take of a throughput sample
    simtime_t throughputSampleTime; // Time between throughput samples
    const char *deliverThrough; // Output gate to deliver packets through

    // Statistics
    cOutVector bufferSizeVector;
    cOutVector packetDropVector;
    unsigned int droppedPackets;
    cOutVector throughput;
    unsigned int lastSampleDelivered; // Amount of packets sent in the last throughputSampleRate
    unsigned int deliveredPackets;
public:
    Queue();
    virtual ~Queue();
protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);

    int getRemainingBuffer();
    virtual cPacket* deliverPacket();
    virtual void sampleThroughput();
    virtual bool attendDataPacket(cMessage *msg);
    virtual void attendFeedbackPacket(cMessage *msg);
};

Define_Module(Queue);

#endif /* QUEUE_H_ */
