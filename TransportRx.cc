#ifndef TRANSPORTRX
#define TRANSPORTRX

#include <string.h>
#include <omnetpp.h>
#include "Queue.h"
#include "SSPPacket_m.h"

using namespace omnetpp;

class TransportRx: public Queue {
private:
    // Congestion control
    unsigned int acknowledgeNumber;
public:
    TransportRx();
    virtual ~TransportRx();
protected:
    virtual void initialize();

    virtual cPacket* deliverPacket();
};

Define_Module(TransportRx);

TransportRx::TransportRx() : acknowledgeNumber(0) {}

TransportRx::~TransportRx() { }

void TransportRx::initialize() {
    Queue::initialize();
    deliverThrough = "toApp";
}

cPacket* TransportRx::deliverPacket() {
    DataPacket *deliveredPacket = static_cast<DataPacket*>(Queue::deliverPacket());
    if (deliveredPacket) { // If a packet was delivered, send feedback
        FeedbackPacket *feedback = new FeedbackPacket();
        feedback->setRemainingBuffer(getRemainingBuffer());
        feedback->setAcknowledgeNumber(deliveredPacket->getSequenceNumber());
        send(feedback, "port$o");
    }
    return deliveredPacket;
}

#endif /* TRANSPORTRX */
